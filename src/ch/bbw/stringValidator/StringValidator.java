package ch.bbw.stringValidator;

import java.util.Scanner;

public class StringValidator {
	public StringValidator() {
		Scanner sc = new Scanner(System.in); //Scanner initialisieren
		String toCheck = sc.nextLine(); //Prüfvariable erstellen und ihr den Eingabewert der Konsole zuweisen
		if (toCheck.contains("@") && toCheck.contains(".")){
			System.out.println("Der eingegebene String ist eine E-Mail Adresse");
		} else if (toCheck.contains(".") && !toCheck.contains("@")){
			System.out.println("Der eingegebene String ist eine IP-Adresse");
		} else if (!toCheck.contains(".") && !toCheck.contains("@") && !toCheck.contains(":")){
			System.out.println("Der eingegebene String ist eine Telefonnummer");
		}
	}
}
